import { Pipe, PipeTransform } from '@angular/core';

enum Days {
  Dimanche,
  Lundi,
  Mardi,
  Mercredi,
  Jeudi,
  Vendredi,
  Samedi
}

enum Months {
  Janvier,
  Février,
  Mars,
  Avril,
  Mai,
  Juin,
  Juillet,
  Août,
  Septembre,
  Octobre,
  Novembre,
  Décembre
}

@Pipe({ name: 'frenchDate' })
export class FrenchDatePipe implements PipeTransform {
  transform(date: Date) {

    const dayOfMonth = date.getDate();
    const nameOfDay = Days[date.getDay()];
    const nameOfMonth = Months[date.getMonth()];
    const year = date.getFullYear();
    const dateComplete = nameOfDay + ' ' + dayOfMonth + ' ' + nameOfMonth + ' ' + year;

    return dateComplete;
  }
}
