import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-resultats',
  templateUrl: './resultats.component.html',
  styleUrls: ['./resultats.component.css']
})
export class ResultatsComponent implements OnInit {

  constructor() { }

  @Input() date: string;
  @Input() meteo: any[];

  pDate: Date;

  isVisible = false;

  ngOnInit() {
    this.pDate = new Date(this.date);
  }

  onIsVisible() {
    this.isVisible = !this.isVisible;
  }

  // translate() {
  //   this.dateFraction = this.date.split(' ');
  //   this.month = this.dateFraction[0];
  //   if (this.month === 'January') {
  //     this.month = 'Janvier';
  //   } else
  //
  // }
}
