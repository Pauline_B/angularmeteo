import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent implements OnInit {

  data: any[];

  // Injection de dépendance
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm) {

      console.log(f.value);
      // tslint:disable-next-line:max-line-length
      this.http.get('http://api.openweathermap.org/data/2.5/forecast?q=' + f.value.search + ',fr&mode=json&appid=1d96f326376f8503f41b36e079a90220&units=metric&lang=fr')
        .subscribe((response: object) => {
          // @ts-ignore
          console.log(response.list);
          // @ts-ignore
//        this.data = response.list;

          const data = [];
          // @ts-ignore
          const list = response.list;
          let date = '';
          let weather = [];
          list.forEach((elem) => {
            if (date !== elem.dt_txt.split(' ')[0]) {
              if (date !== '') {
                data.push({date, weather});
                weather = [];
              }
              date = elem.dt_txt.split(' ')[0];
            }
            weather.push(elem);
          });
          console.log(data);
          this.data = data;
        });
    }
}
