import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  @Input() date: string;
  @Input() temperature: number;
  @Input() description: string;
  @Input() icon: string;
  @Input() pressure: number;
  @Input() humidity: number;
  @Input() windSpeed: number;

  constructor() {}

  ngOnInit() {
    const hours = this.date.split(' ')[1].split(':');
    this.date = hours[0] + ' : ' + hours[1];
  }




}
